/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.shehuan.nicedialog;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Optional;

/**
 * PxUtils 像素转换工具类
 *
 * @author zf
 * @since 2021-05-10
 */
public class PxUtils {
    /**
     * vp转px
     *
     * @param context 上下文
     * @param vp      vp
     * @return px 像素
     */
    public static int vp2px(Context context, float vp) {
        Optional var1 = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes var2 = ((Display) var1.get()).getAttributes();
        float dpi = var2.scalDensity;
        return (int) (vp * dpi);
    }

    /**
     * 获取屏幕密度
     *
     * @param context 上下文
     * @return densityPixels 屏幕密度
     */
    public static float getScreenScale(Context context) {
        Optional var1 = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes var2 = ((Display) var1.get()).getAttributes();
        return var2.densityPixels;
    }
}
