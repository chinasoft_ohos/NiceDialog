package com.shehuan.nicedialog.slice;

import com.shehuan.nicedialog.*;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.ToastDialog;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import static ohos.agp.animation.Animator.INFINITE;

/**
 * 首页
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        ((Button) findComponentById(ResourceTable.Id_showDialog0)).setClickedListener(this);
        ((Button) findComponentById(ResourceTable.Id_showDialog1)).setClickedListener(this);
        ((Button) findComponentById(ResourceTable.Id_showDialog2)).setClickedListener(this);
        ((Button) findComponentById(ResourceTable.Id_showDialog3)).setClickedListener(this);
        ((Button) findComponentById(ResourceTable.Id_showDialog4)).setClickedListener(this);
        ((Button) findComponentById(ResourceTable.Id_showDialog5)).setClickedListener(this);
        ((Button) findComponentById(ResourceTable.Id_showDialog6)).setClickedListener(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_showDialog0:
                showShareDialog();
                break;
            case ResourceTable.Id_showDialog1:
                showCommitDialog(ResourceTable.Layout_friend_set_layout);
                break;
            case ResourceTable.Id_showDialog2:
                showCommitDialog(ResourceTable.Layout_commit_layout);
                break;
            case ResourceTable.Id_showDialog3:
                showAdDialog();
                break;
            case ResourceTable.Id_showDialog4:
                showLoadingDialog();
                break;
            case ResourceTable.Id_showDialog5:
                showConfirmDialog("提示", "您已支付成功");
                break;
            case ResourceTable.Id_showDialog6:
                showConfirmDialog("警告", "您的账号已被冻结");
                break;
        }
    }

    private void showConfirmDialog(String title, String content) {
        NiceDialog.init(getContext())
                .setLayoutId(ResourceTable.Layout_confirm_layout, ConstandCodes.CONFRIMTYPE)
                .setViewConvertListener(new ViewConvertListener() {
                    @Override
                    public void convertView(ViewHolder holder, NiceDialog dialog) {
                        holder.setText(ResourceTable.Id_title, title);
                        holder.setText(ResourceTable.Id_message, content);
                        holder.setOnClickListener(ResourceTable.Id_dl_ok, new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                showToast("确定");
                                dialog.hide();
                            }
                        });
                        holder.setOnClickListener(ResourceTable.Id_dl_cancel, new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                showToast("取消");
                                dialog.hide();
                            }
                        });
                    }
                })
                .setTransparent(true)
                .show();
    }

    private void showLoadingDialog() {
        int size = PxUtils.vp2px(getContext(), 150);
        NiceDialog.init(getContext())
                .setLayoutId(ResourceTable.Layout_loading_layout, ConstandCodes.COMMONTYPE)
                .setViewConvertListener(new ViewConvertListener() {
                    @Override
                    public void convertView(ViewHolder holder, NiceDialog dialog) {
                        EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());
                        eventHandler.postTask(new Runnable() {
                            @Override
                            public void run() {
                                holder.getView(ResourceTable.Id_round_progress_bar).createAnimatorProperty()
                                        .rotate(360).setLoopedCount(INFINITE)
                                        .setDuration(800).start();
                            }
                        }, 50);
                    }
                })
                .setSwipeToClose(true)
                .setSize(size, size)
                .setTransparent(true)
                .show();
    }

    private void showAdDialog() {
        NiceDialog.init(getContext())
                .setLayoutId(ResourceTable.Layout_ad_layout, ConstandCodes.COMMONTYPE)
                .setViewConvertListener(new ViewConvertListener() {
                    @Override
                    public void convertView(ViewHolder holder, NiceDialog dialog) {
                        holder.setOnClickListener(ResourceTable.Id_img_close, new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                dialog.hide();
                            }
                        });
                    }
                })
                .setTransparent(true)
                .show();
    }

    private void showCommitDialog(int layout) {
        NiceDialog.init(getContext())
                .setLayoutId(layout, ConstandCodes.COMMONTYPE)
                .setViewConvertListener(new ViewConvertListener() {
                    @Override
                    public void convertView(ViewHolder component, NiceDialog dialog) {
                    }
                })
                .setGravity(ConstandCodes.BOTTOM)
                .setSwipeToClose(true)
                .show();
    }

    private void showShareDialog() {
        NiceDialog.init(getContext())
                .setLayoutId(ResourceTable.Layout_share_layout, ConstandCodes.COMMONTYPE)
                .setViewConvertListener(new ViewConvertListener() {
                    @Override
                    public void convertView(ViewHolder holder, NiceDialog dialog) {
                        holder.setOnClickListener(ResourceTable.Id_wechat, new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                showToast("分享成功");
                            }
                        });
                    }
                })
                .setGravity(ConstandCodes.BOTTOM)
                .setSwipeToClose(true)
                .show();
    }

    private void showToast(String msg) {
        ToastDialog toastDialog = new ToastDialog(getContext());
        toastDialog.setText(msg).show();
    }
}
