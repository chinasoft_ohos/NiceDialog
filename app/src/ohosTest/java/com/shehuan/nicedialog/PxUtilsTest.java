/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.shehuan.nicedialog;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;

import org.junit.Test;

/**
 * PxUtils单元测试
 *
 * @author zf
 * @since 2021-06-10
 */
public class PxUtilsTest {
    private Context context;

    /**
     * 获取context
     */
    public void initClass() {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility().getContext();
    }

    /**
     * vp转px
     */
    @Test
    public void vpToPx() {
        initClass();
        int px = PxUtils.vp2px(context, 150);
    }

    /**
     * 获取屏幕密度
     */
    @Test
    public void getScreenScale() {
        initClass();
        float screenScale = PxUtils.getScreenScale(context);
    }
}