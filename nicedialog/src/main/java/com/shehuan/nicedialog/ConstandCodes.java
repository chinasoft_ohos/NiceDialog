/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.shehuan.nicedialog;

/**
 * 常量类
 */
public class ConstandCodes {
    /**
     * 顶部
     */
    public static final int TOP = 1;
    /**
     * 底部
     */
    public static final int BOTTOM = 2;
    /**
     * 左边
     */
    public static final int LEFT = 4;
    /**
     * 右边
     */
    public static final int RIGHT = 5;
    /**
     * 中间
     */
    public static final int CENTER = 6;
    /**
     * 一般样式
     */
    public static final int COMMONTYPE = 7;
    /**
     * 提交样式
     */
    public static final int CONFRIMTYPE = 8;
}
