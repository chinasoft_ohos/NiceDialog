package com.shehuan.nicedialog;

/**
 * dialog对外接口
 */
public interface ViewConvertListener {
    /**
     * 转换
     *
     * @param holder view绑定者
     * @param dialog 展示的dialog
     */
    void convertView(ViewHolder holder, NiceDialog dialog);
}
