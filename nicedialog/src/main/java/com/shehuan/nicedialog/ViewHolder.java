package com.shehuan.nicedialog;

import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.HashMap;
import java.util.Map;

/**
 * view转换类
 */
public class ViewHolder {
    private final Context context;
    private Map<Integer, Component> views;
    private Component convertView;

    private ViewHolder(Component view, Context context) {
        convertView = view;
        this.context = context;
        views = new HashMap<>();
    }

    /**
     * 创建ViewHolder
     *
     * @param view    控件
     * @param context 上下文
     * @return ViewHolder 控件持有
     */
    public static ViewHolder create(Component view, Context context) {
        return new ViewHolder(view, context);
    }

    /**
     * 获取控件
     *
     * @param viewId 控件ID
     * @param <T>    泛型
     * @return Component 控件
     */
    public <T extends Component> T getView(int viewId) {
        Component view = views.get(viewId);
        if (view == null) {
            view = convertView.findComponentById(viewId);
            views.put(viewId, view);
        }
        return (T) view;
    }

    /**
     * 设置文本内容
     *
     * @param viewId 控件ID
     * @param text   文本String类型
     */
    public void setText(int viewId, String text) {
        Text textView = getView(viewId);
        textView.setText(text);
    }

    /**
     * 设置文本内容
     *
     * @param viewId 控件ID
     * @param textId 文本资源ID
     */
    public void setText(int viewId, int textId) {
        Text textView = getView(viewId);
        textView.setText(textId);
    }

    /**
     * 设置文本颜色
     *
     * @param viewId 控件ID
     * @param color  颜色
     */
    public void setTextColor(int viewId, Color color) {
        Text textView = getView(viewId);
        textView.setTextColor(color);
    }

    /**
     * 设置点击事件监听
     *
     * @param viewId        控件ID
     * @param clickListener 监听器
     */
    public void setOnClickListener(int viewId, Component.ClickedListener clickListener) {
        Component view = getView(viewId);
        view.setClickedListener(clickListener);
    }

    /**
     * 设置文本内容背景
     *
     * @param viewId 控件ID
     * @param resId  element资源ID
     */
    public void setBackgroundResource(int viewId, int resId) {
        Component view = getView(viewId);
        view.setBackground(new ShapeElement(context, resId));
    }
}
