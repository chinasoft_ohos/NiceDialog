package com.shehuan.nicedialog;

import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.Window;
import ohos.app.Context;

/**
 * Dialog封装类
 */
public class NiceDialog extends CommonDialog {
    /**
     * 上下文
     */
    protected final Context myContext;
    private int layout;
    private int type;
    private boolean isEnabled;

    protected NiceDialog(Context context) {
        super(context);
        myContext = context;
    }

    /**
     * 初始化dialog
     *
     * @param context 上下文
     * @return NiceDialog 自定义Dialog
     */
    public static NiceDialog init(Context context) {
        return new NiceDialog(context);
    }

    /**
     * 设置gravity
     *
     * @param gravity ConstandCodes常量
     * @return NiceDialog 自定义Dialog
     */
    public NiceDialog setGravity(int gravity) {
        switch (gravity) {
            case ConstandCodes.TOP:
                break;
            case ConstandCodes.BOTTOM:
                Window window = getWindow();
                window.setLayoutAlignment(LayoutAlignment.BOTTOM);
                break;
            case ConstandCodes.LEFT:
                break;
            case ConstandCodes.RIGHT:
                break;
            case ConstandCodes.CENTER:
                break;
        }
        return this;
    }

    /**
     * 设置Dialog布局接口
     *
     * @param viewConvertListener Dialog布局对外暴露接口
     * @return NiceDialog 自定义Dialog
     */
    public NiceDialog setViewConvertListener(ViewConvertListener viewConvertListener) {
        this.viewConvertListener = viewConvertListener;
        return this;
    }

    private ViewConvertListener viewConvertListener;

    /**
     * 设置layout资源文件
     *
     * @param layout_share_layout 资源ID
     * @param type                dialog类型
     * @return NiceDialog 自定义Dialog
     */
    public NiceDialog setLayoutId(int layout_share_layout, int type) {
        this.layout = layout_share_layout;
        this.type = type;
        return this;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        Component shareLayout = LayoutScatter.getInstance(myContext)
                .parse(layout, null, true);
        ViewHolder viewHolder = covertHolder(shareLayout, myContext);
        if (viewConvertListener != null) {
            viewConvertListener.convertView(viewHolder, this);
        }
        if (type == ConstandCodes.CONFRIMTYPE) {
            int height = shareLayout.getLayoutConfig().height;
            setSize(800, height);
        }
        setDialogListener(new DialogListener() {
            @Override
            public boolean isTouchOutside() {
                if (isEnabled) {
                    destroy();
                }
                return true;
            }
        });
        setContentCustomComponent(shareLayout);
    }

    /**
     * 设置是否点击外部关闭dialog
     *
     * @param isEnabled false为不关闭 true为关闭
     * @return NiceDialog 自定义Dialog
     */
    public NiceDialog setSwipeToClose(boolean isEnabled) {
        this.isEnabled = isEnabled;
        return this;
    }

    private ViewHolder covertHolder(Component view, Context context) {
        ViewHolder viewHolder = ViewHolder.create(view, context);
        return viewHolder;
    }
}

