# NiceDialog


#### 项目介绍
- 项目名称：NiceDialog
- 所属系列：openharmony的第三方组件适配移植
- 功能：NiceDialog基于CommonDialog的扩展，让dialog的使用更方便
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 1.2.0

#### 效果演示
![输入图片说明](https://images.gitee.com/uploads/images/2021/0526/111810_f6b9b58a_2291343.gif "SVID_20210526_111029_1.gif")

#### 安装教程



1.在项目根目录下的build.gradle文件中，

 ```
allprojects {

    repositories {

        maven {

            url 'https://s01.oss.sonatype.org/content/repositories/releases/'

        }
    }
}

 ```

2.在entry模块的build.gradle文件中，

 ```

 dependencies {

        implementation('com.gitee.chinasoft_ohos:NiceDialog:1.2.1')


    ......  

 }

 ```
在sdk5，sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
1.配置、展示基础dialog
```java
NiceDialog.init(this)
        .setLayoutId(ResourceTable.Layout_loading_layout, ConstandCodes.COMMONTYPE)//设置dialog布局文件和类型 
        .setViewConvertListener(new ViewConvertListener() {//进行相关View操作的回调
             @Override
             public void convertView(ViewHolder holder, NiceDialog dialog) {
             }
        .setSwipeToClose(true)//控制点击外部是否关闭dialog
        .setSize(450, 450)//设置dialog宽高
        .setTransparent(true)//设置dialog透明度
        .setGravity(BOTTOM)//设置dialog位置
        .show();//展示dialog
```
2.配置、展示自定义dialog
```java
NiceDialog.init(this)
       .setLayoutId( ResourceTable.Layout_confirm_layout, ConstandCodes.CONFRIMTYPE)
       .setViewConvertListener(new ViewConvertListener() {
            @Override
            public void convertView(ViewHolder holder, NiceDialog dialog) {
            holder.setText(ResourceTable.Id_title, "提示");
            holder.setText(ResourceTable.Id_message, "您已支付成功");
            holder.setOnClickListener(ResourceTable.Id_dl_ok, new Component.ClickedListener() {
                  @Override
                  public void onClick(Component component) {
                         showToast("确定");
                         dialog.hide();
                  }
            });
            holder.setOnClickListener(ResourceTable.Id_dl_cancel, new Component.ClickedListener() {
                   @Override
                   public void onClick(Component component) {
                          showToast("取消");
                          dialog.hide();
                        }
                   });

            }
         })
        .setTransparent(true)
        .show();
```


#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代
- 1.2.1
- 0.0.1-SNAPSHOT
